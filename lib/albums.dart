import 'package:first_app/album.dart';
import 'package:first_app/album_detail.dart';
import 'package:first_app/http_service.dart';
import 'package:flutter/material.dart';

class AlbumPage extends StatelessWidget {
  final HttpService httpService = HttpService();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Albums'),
      ),
      body: FutureBuilder(
        future: httpService.getPosts(),
        builder: (BuildContext context, AsyncSnapshot<List<Album>> snapshot) {
          if (snapshot.hasData) {
            List<Album> albums = snapshot.data;

            return ListView(
              children: albums
                  .map((Album album) => ListTile(
                        title: Text(album.title),
                        subtitle: Text(album.body),
                        onTap: () => Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => (AlbumDetail(
                              album: album,
                            )),
                          ),
                        ),
                      ))
                  .toList(),
            );
          }

          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}

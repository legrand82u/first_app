import 'dart:convert';

import 'package:first_app/album.dart';
import 'package:http/http.dart';

class HttpService {
  final String postUrl = "https://jsonplaceholder.typicode.com/posts";

  Future<List<Album>> getPosts() async {
    Response res = await get(postUrl);

    if (res.statusCode == 200) {
      List<dynamic> body = jsonDecode(res.body);

      List<Album> albums =
          body.map((dynamic item) => Album.fromJson(item)).toList();

      return albums;
    } else {
      throw "Can't load Albums";
    }
  }

  Future<void> deleteAlbum(int id) async {
    Response res = await delete("$postUrl/$id");
    try {
      if (res.statusCode == 200) {
        print("Deleted !");
      }
    } catch (e) {
      print(e);
    }
  }
}

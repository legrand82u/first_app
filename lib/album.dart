//On crée un album, afin de traduire ce qu'on reçoit en JSON en une classe Dart utilisable.
import 'package:flutter/material.dart';

class Album extends StatelessWidget{
  final int userId;
  final int id;
  final String title;
  final String body;

  Album({this.userId, this.id, this.title, this.body});

  @override
  Widget build(BuildContext context) {
    return Container(
     child: Text(title),
    );
  }

  //L'usine permettant de traduire le Json qu'on reçoit (qui est une map de clé sous forme de chaines de caractères et d'on ne sait trop quoi.)
  factory Album.fromJson(Map<String, dynamic> json) {
    return Album(
      userId: json['userId'] as int,
      id: json['id'] as int,
      title: json['title'] as String,
      body: json['body'] as String,
    );
  }
}

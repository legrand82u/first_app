import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final int resultScore;
  final Function resetQuiz;

  Result(this.resultScore, this.resetQuiz);

  String get resultPhrase {
    var resultText = 'You did it !';

    if (resultScore <= 80) {
      resultText = 'You are awesome !';
    } else if (resultScore <= 120) {
      resultText = 'Pretty likeable !';
    } else if (resultScore <= 160) {
      resultText = 'You\'re strange !';
    } else {
      resultText = 'You\'re so bad !';
    }

    return resultText;
  }

  //On crée un "result" widget, qui contient un texte, et, un bouton.
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: <Widget>[
          Text(
            resultPhrase,
            style: TextStyle(fontSize: 36, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
          FlatButton(
            child: Text('Restart Quiz!'),
            onPressed: resetQuiz,
            textColor: Colors.blue,
          )
        ],
      ),
    );
  }
}

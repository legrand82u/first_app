import 'package:first_app/album.dart';
import 'package:first_app/http_service.dart';
import 'package:flutter/material.dart';

class AlbumDetail extends StatelessWidget {
  final Album album;
  final HttpService httpService = HttpService();

  AlbumDetail({@required this.album});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(album.title),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.delete),
        onPressed: () {
          httpService.deleteAlbum(album.id);
          Navigator.pop(context);
        },
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            children: <Widget>[
              ListTile(
                title: Text("Title"),
                subtitle: Text(album.title),
              ),
              ListTile(
                title: Text("ID"),
                subtitle: Text("${album.id}"),
              ),
              ListTile(
                title: Text("Body"),
                subtitle: Text(album.body),
              ),
              ListTile(
                title: Text("UserID"),
                subtitle: Text("${album.userId}"),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

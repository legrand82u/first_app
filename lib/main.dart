import 'package:first_app/albums.dart';
import 'package:flutter/material.dart';

import './quiz.dart';
import './result.dart';
import './album.dart';

// void main() {
//  runApp(MyApp());
// }

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  var _questionIndex = 0;
  var _totalScore = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  final _questions = const [
    {
      'questionText': 'What`s your favorite color ?',
      'answers': [
        {'text': 'Black', 'score': 10},
        {'text': 'Blue', 'score': 20},
        {'text': 'White', 'score': 30},
        {'text': 'Red', 'score': 40}
      ],
    },
    {
      'questionText': 'What`s your favorite animal ?',
      'answers': [
        {'text': 'Dog', 'score': 10},
        {'text': 'Cat', 'score': 20},
        {'text': 'Lion', 'score': 30},
        {'text': 'Rabbit', 'score': 40}
      ],
    },
    {
      'questionText': 'What`s your favorite drink ?',
      'answers': [
        {'text': 'Coke', 'score': 10},
        {'text': 'Water', 'score': 20},
        {'text': 'Milk', 'score': 30},
        {'text': 'Tea', 'score': 40}
      ],
    },
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter HTTP',
      debugShowCheckedModeBanner: false,
      home: AlbumPage(),
      // home: Scaffold(
      //   appBar: AppBar(
      //     title: Text('My First App'),
      //   ),
      //Pour voir la première version, il suffit de décommenter le tout, jusqu'à result, et de commenter l'autre body
      // body: _questionIndex < _questions.length
      ////On crée un quiz qui nécessite un pointeur vers une fontion, les questions (avec réponses, et score), et, l'index des questions (où on en est dans la liste)
      // ? Quiz(
      //     answerQuestion: _answerQuestion,
      //     questions: _questions,
      //     questionIndex: this._questionIndex)
      // //On crée un result widget, qui, nécessite le score total, et, le pointeur vers la fonction de reset du quiz
      // : Result(_totalScore, resetQuiz),
    );
  }

  void _answerQuestion(int score) {
    _totalScore += score;
    setState(() {
      _questionIndex++;
    });
    print('Answer chosen : ');
    print(_questionIndex);
  }

  void resetQuiz() {
    setState(() {
      _questionIndex = 0;
      _totalScore = 0;
    });
  }

  //Fonction qui réalise le get, et qui retourne une Future, quasiment équivalente aux Promises en JS
  // Future<List<Album>> fetchAlbum() async {
  //   final response =
  //       await http.get('https://jsonplaceholder.typicode.com/posts');

  //   if (response.statusCode == 200) {
  //     var listAlbums = [];
  //    json.decode(response.body).map((elem) {
  //      listAlbums.add(Album(title: elem.title, id: elem.id, userId: elem.userId, body: elem.body));
  //    });
  //    return listAlbums;
  //   } else {
  //     throw "Error while trying to get.";
  //   }
  // }
}

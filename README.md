# Premiers pas

Pour la première version de l'application (sans le HTPP), le principal, et quasiment unique support a été la vidéo youtube très longue :  'https://www.youtube.com/watch?v=x0uinJvhNxI'  

# Requêtes HTTP

Pour la suite, afin de mieux comprendre les requêtes HTTP, je me suis aidé d'une vidéo (très courte, la personne survole beaucoup ce qu'il montre) :  'https://www.youtube.com/watch?v=wr8WsNVxybY'  Mais aussi du site officiel de flutter : 'https://flutter.dev/docs/cookbook/networking/fetch-data'  

Le code n'est pas commenté, ou très peu, sur mes zones d'ombres, il ne faut pas hésiter à venir me voir si questions il y a.